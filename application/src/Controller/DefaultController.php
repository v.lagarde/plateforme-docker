<?php
/**
 * Created by PhpStorm.
 * User: Lagarde
 * Date: 19/10/2018
 * Time: 08:46
 */
namespace App\Controller;

use App\Repository\ArticleRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
class DefaultController extends AbstractController
{
    private $articleRepository;
    public function __construct(ArticleRepository $articleRepository)
    {
        $this->articleRepository = $articleRepository;
    }

    /**
     * @Route("/", name="home")
     */
    public function index()
    {
        return $this->render('default/home.html.twig',[
            'articleList' => $this->articleRepository->findAll()
        ]);
    }

}